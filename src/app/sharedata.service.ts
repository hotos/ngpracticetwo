import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IMyModel } from './home/interface.models';

@Injectable({
  providedIn: 'root'
})
export class SharedataService {
data: IMyModel;
modelArrays: IMyModel[] = [];
 private datasender = new BehaviorSubject<IMyModel>(this.data);
 private arraysender = new BehaviorSubject<IMyModel[]>(this.modelArrays);

 currentData = this.datasender.asObservable();
 actualArray = this.arraysender.asObservable();
 shareData(data: IMyModel) {
   this.datasender.next(data);
   this.modelArrays.push(data);
 }
 shareArray(myarray: IMyModel[]) {
  this.arraysender.next(myarray);
 }
  constructor() { }
}
