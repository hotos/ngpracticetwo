import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostcompComponent } from './postcomp.component';

describe('PostcompComponent', () => {
  let component: PostcompComponent;
  let fixture: ComponentFixture<PostcompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostcompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostcompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
