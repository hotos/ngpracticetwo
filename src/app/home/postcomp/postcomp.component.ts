import { Component, OnInit } from '@angular/core';
import { SharedataService } from 'src/app/sharedata.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-postcomp',
  template: `
  <form [formGroup]="setForm" (ngSubmit)="onSubmit()">
  <div class="grid-container">
    <div class="grid-x grid-padding-x">
      <div class="cell">
        <label>Nombre Completo
          <input type="text" formControlName="nameval" [ngClass]="{'is-invalid':submitted && f.nameval.errors}"
          >
          <div *ngIf="submitted && f['nameval'].errors" class="alert label">
              <div *ngIf="f['nameval'].errors.required">El campo Nombre Completo es necesario</div>
          </div>
        </label>
        <label>Direccion
          <input type="text" formControlName="addressval" [ngClass]="{'is-invalid':submitted && f.addressval.errors}"
         >
          <div *ngIf="submitted && f['addressval'].errors" class="alert label">
              <div *ngIf="f['addressval'].errors.required">La Direccion es necesaria</div>
          </div>
        </label>
        <label>Teleono
          <input type="number" formControlName="phone" [ngClass]="{'is-invalid':submitted && f.phoneval.errors}"
          >
          <div *ngIf="submitted && f['phoneval'].errors" class="alert label">
              <div *ngIf="f['phoneval'].errors.required">Proporcione la edad</div>
          </div>
        </label>
        <label>Correo
          <input type="text" formControlName="emailval" [ngClass]="{'is-invalid':submitted && f.emailval.errors}"
          >
          <div *ngIf="submitted && f['emailval'].errors" class="alert label">
              <div *ngIf="f['emailval'].errors.required">Proporcione un correo</div>
              <div *ngIf="f['emailval'].errors.email">Proporcione un correo valido</div>
          </div>
        </label>
        <button class="button" type="submit">Agregar</button>
      </div>
    </div>
  </div>
</form>
  `,
  styleUrls: ['./postcomp.component.scss']
})
export class PostcompComponent implements OnInit {
  private data;
  submitted = false;
  setForm: FormGroup;
  constructor(private dataMng: SharedataService, private http: HttpClient, public builderVal: FormBuilder) { }
  postData(id: number) {
    const jsonpost = 'https://my-json-server.typicode.com/mavericc/demo/perfiles/';
    const header = new HttpHeaders({
      'Content-type': 'application/json'
    });
    return this.http.post(jsonpost, this.data, {headers: header})
    .subscribe(
        (val) => {
            console.log('POST call successful value returned in body',
                        val);
        },
        response => {
            console.log('POST call in error', response);
        },
        () => {
            console.log('The POST observable is now completed.');
        });
  }
  get f() { return this.setForm.controls; }
  createData() {
    this.data = {
      id: this.randomIntFromInterval(10, 100),
      name: this.setForm.value.nameval,
      email: this.setForm.value.emailval,
      address: this.setForm.value.addressnameval,
      profile_pic: 'https://robohash.org/DRS.png?size=200x200&set=set1',
      phone: this.setForm.value.phoneval,
      };
  }
  ngOnInit(): void {
    this.setForm = this.builderVal.group({
      nameval: ['', Validators.required],
      addressnameval: ['', Validators.required],
      phoneval: ['', Validators.required],
      emailval: ['', [Validators.required, Validators.email]],
    },
    {

    }
    );
    }
    private randomIntFromInterval(min, max) {
      return Math.floor(Math.random() * (max - min + 1 ) + min);
  }
    onSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.setForm.invalid) {
        return;
      }
      this.submitted = false;
      this.setForm.reset();
    }
}
