import { Component, OnInit, NgModule, Input, OnChanges } from '@angular/core';
import { SharedataService } from 'src/app/sharedata.service';
import { IMyModel } from '../interface.models';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  // singleRow: IMyModel;
  myarray: IMyModel[] = [];
  // @Input() myarray = [];
  constructor(private data: SharedataService) {
   }
  ngOnInit() {
this.data.actualArray.subscribe(arraydata => this.myarray = arraydata);
// this.myarray.push(this.singleRow);
  }
}
