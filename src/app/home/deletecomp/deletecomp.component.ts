import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-deletecomp',
  template: `
  <h1>Delete Https</h1>
  <table>
  <thead>
    <tr>
      <th width="200">Perfil</th>
      <th width="150">Nombre</th>
      <th width ="150">Correo</th>
      <th width="150">Direccion</th>
      <th width="150">Telefono</th>
      <th width="150">Accion</th>
    </tr>
  </thead>
  <tbody *ngFor="let profile of profiles">
    <tr>
      <td> <img src='{{profile.profile_pic}}'></td>
      <td>{{profile.name}}</td>
      <td>{{profile.email}}</td>
      <td>{{profile.address}}</td>
      <td>{{profile.telephone}}</td>
      <td><button class="button" (click)="deleteData(profile.id)"> Delete Data</button></td>
    </tr>
  </tbody>
</table>
  `,
  styleUrls: ['./deletecomp.component.scss']
})
export class DeletecompComponent implements OnInit {

  constructor(private http: HttpClient) { }
  private urljson = 'https://my-json-server.typicode.com/sotoh/justjsondb/profiles';
  profiles = [];
  ngOnInit() {
    this.getData();
  }
  getData() {
    return this.http.get(this.urljson).subscribe(
      (resp: any[]) => {
        this.profiles = resp;
      });
  }
  deleteData(id: number) {
    const jsondelete = 'https://my-json-server.typicode.com/sotoh/justjsondb/profiles/' + id;
    const header = new HttpHeaders({
      'Content-type': 'application/json'
    });
    this.profiles.filter(item => item.id !== id);
    console.log(this.profiles.length);
    return this.http.delete(jsondelete, {headers: header}).subscribe(
      (val) => {
          console.log('DELETE call successful value returned in body',
                      val);
      },
      response => {
          console.log('DELETE call in error', response);
      },
      () => {
          console.log('The DELETE observable is now completed.');
      });
    // this.profiles.splice(id, 1);
    // this.list = this.list.filter(item => item.id !== id);
  }
}
