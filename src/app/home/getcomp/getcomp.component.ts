import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-getcomp',
  template: `
  <h1>Get Https</h1>
  <button class="button" (click)="getData()"> Get Data</button>
  <table>
  <thead>
    <tr>
      <th width="200">Perfil</th>
      <th width="150">Nombre</th>
      <th width ="150">Correo</th>
      <th width="150">Direccion</th>
      <th width="150">Telefono</th>
    </tr>
  </thead>
  <tbody *ngFor="let profile of profiles">
    <tr>
      <td> <img src='{{profile.profile_pic}}'></td>
      <td>{{profile.name}}</td>
      <td>{{profile.email}}</td>
      <td>{{profile.address}}</td>
      <td>{{profile.telephone}}</td>
    </tr>
  </tbody>
</table>
  `,
  styleUrls: ['./getcomp.component.scss']
})
export class GetcompComponent implements OnInit {

  constructor(private http: HttpClient) { }
  private urljson = 'https://my-json-server.typicode.com/sotoh/justjsondb/profiles';
  profiles = [];
  ngOnInit() {
  }
  getData() {
    return this.http.get(this.urljson).subscribe(
      (resp: any[]) => {
        this.profiles = resp;
      });
  }
}
