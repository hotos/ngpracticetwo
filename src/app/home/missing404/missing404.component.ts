import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-missing404',
  templateUrl: './missing404.component.html',
  styleUrls: ['./missing404.component.scss']
})
export class Missing404Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
