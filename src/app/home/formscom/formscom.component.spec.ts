import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormscomComponent } from './formscom.component';

describe('FormscomComponent', () => {
  let component: FormscomComponent;
  let fixture: ComponentFixture<FormscomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormscomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormscomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
