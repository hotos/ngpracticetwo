import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { GetcompComponent } from './home/getcomp/getcomp.component';
import { PostcompComponent } from './home/postcomp/postcomp.component';
import { DeletecompComponent } from './home/deletecomp/deletecomp.component';
import { Missing404Component } from './home/missing404/missing404.component';
import { FormscomComponent } from './home/formscom/formscom.component';
import { TableComponent } from './home/table/table.component';

const routes: Routes = [
  {path: '', component: TableComponent},
  {path: 'get', component: GetcompComponent},
  {path: 'post', component: PostcompComponent},
  {path: 'delete', component: DeletecompComponent},
  {path: '**', component: Missing404Component}
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(
      routes,
      { enableTracing: true }
    )],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
