import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TableComponent } from './home/table/table.component';
import { AlertModule } from 'ngx-foundation';
import { AccordionModule } from 'ngx-foundation';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { GetcompComponent } from './home/getcomp/getcomp.component';
import { DeletecompComponent } from './home/deletecomp/deletecomp.component';
import { PostcompComponent } from './home/postcomp/postcomp.component';
import { AppRoutingModule } from './app-routing.module';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { Missing404Component } from './home/missing404/missing404.component';
import { FormscomComponent } from './home/formscom/formscom.component';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    GetcompComponent,
    DeletecompComponent,
    PostcompComponent,
    Missing404Component,
    FormscomComponent
  ],
  imports: [
    BrowserModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    AlertModule.forRoot(),
    AccordionModule.forRoot(),
    FormsModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
