import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { IMyModel } from './home/interface.models';
import { SharedataService } from './sharedata.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  // @Output() senderEvent = new EventEmitter<IMyModel[]>();
  // @Output() senderEvent = new EventEmitter<IMyModel>();
  title = 'ngzurb';

  validatorForm: FormGroup;
  submitted = false;
  singleRow: IMyModel;
  array: IMyModel[] = [{name: 'Hector', lastname: 'Soto', slastname: 'Hernandez', age: 25,
  genre: 'H', icon: 1, email: 'alguien@example.com'}];
  options = [{name: 'option1', value: 'H'}, {name: 'option2', value: 'M'}];
  myarray: Array<IMyModel> = new Array<IMyModel>();
   // min and max included
  private randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1 ) + min);
}
 // Validators
// Function to fill in array
  private filltable() {
    this.array.push({
      name: this.validatorForm.value.nameval,
      lastname: this.validatorForm.value.lastnameval,
      slastname: this.validatorForm.value.slastnameval,
      age: this.validatorForm.value.ageval,
      genre: this.validatorForm.controls.genreval.value, // this.myForm.controls["selectFormCtrl"].value
      icon: this.randomIntFromInterval(1, 10),
      email: this.validatorForm.value.emailval,
    });
   }
   // Function to get to the Model
   private getSingleData() {
    this.singleRow = {
    name: this.validatorForm.value.nameval,
    lastname: this.validatorForm.value.lastnameval,
    slastname: this.validatorForm.value.slastnameval,
    age: this.validatorForm.value.ageval,
    genre: this.validatorForm.value.genreval.value,
    icon: this.randomIntFromInterval(1, 10),
    email: this.validatorForm.value.emailval
    };
    // this.array.push(this.singleRow);
    this.mydata.shareData(this.singleRow);
   }
   constructor(public builderVal: FormBuilder, private mydata: SharedataService) {
   }
   get f() { return this.validatorForm.controls; }
   get genre() { return this.validatorForm.get('genreval'); }
   ngOnInit(): void {
    this.validatorForm = this.builderVal.group({
      nameval: ['', Validators.required],
      lastnameval: ['', Validators.required],
      slastnameval: ['', Validators.required],
      ageval: ['', Validators.required],
      genreval: ['', Validators.required],
      emailval: ['', [Validators.required, Validators.email]],
    },
    {

    }
    );
    this.mydata.currentData.subscribe(modelData => this.singleRow = modelData);
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.validatorForm.invalid) {
      console.log(this.array);
      // console.log(this.singleRow);
      return;
    }
    // this.filltable();
    this.getSingleData();
    this.submitted = false;
    this.validatorForm.reset();
    // console.log(this.array);
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.validatorForm.value));
  }

}
